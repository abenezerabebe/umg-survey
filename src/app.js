// npm packages
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import passport from 'passport';
import morgan from 'morgan';
import cors from 'cors';
import serveIndex from 'serve-index';

// proj packages
import { logger } from './util';
import { auth as authConfig } from '../config';
import setupAuthRoutes from './auth';
import setupSpotifyRoutes from './spotify';
import setupUserRoutes from './user';

// init app
const app = express();

const isLoggedIn = (req, res, next) => {
  if (req.user) return next();
  return res.redirect('/login');
};
// setup logging
app.use(morgan('combined', { stream: logger.stream }));

// setup CORS
app.use(cors());

// add body parsing
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// add cookie parsing
app.use(cookieParser());

// add session support
app.use(session({
  secret: authConfig.sessionSecret,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false },
}));

// set up static files
app.set('view engine', 'ejs');
app.use(express.static('./'));

// setup passport.js
app.use(passport.initialize());
app.use(passport.session());
app.use('/data', isLoggedIn, serveIndex('./data'));
// test method
app.get('/', (req, res) => {
  if (req.query.id) app.set('userId', req.query.id);
  else app.set('userId', 'test');
  res.redirect('/api/auth/spotify');
});

// TODO: remove from app file
app.get('/thank-you', (req, res) => (
  res.render(`${__dirname}/../views/index`, { trackId: app.get('trackId') })
));

// TODO: remove from app file
app.get('/login', (req, res) => (
  res.render(`${__dirname}/../views/login`)
));

// setup authentication routes
setupAuthRoutes(app);
// setup spotify API routes
setupSpotifyRoutes(app);
// setup user routes
setupUserRoutes(app);

export default app;
