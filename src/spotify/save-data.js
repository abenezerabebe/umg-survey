import { writeCSV, apiRequest, mapper } from '../util';

export const saveData = items => (
  Promise.all(items.map(async ({ name, request, map }) => {
    let collection = await apiRequest(request);
    if (!collection) return { name, result: !!collection };
    collection = mapper(map, collection);
    return writeCSV(collection, name);
  }))
);
