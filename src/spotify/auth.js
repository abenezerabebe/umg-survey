import passport from 'passport';
import { getTopTrack } from './top-track';
import { saveData } from './save-data';
import { spotify as config } from '../../config';
import { writeCSV, logger } from '../util';

export default (app) => {
  app.get('/api/auth/spotify', passport.authenticate('spotify', { scope: config.scope }));

  app.get('/api/auth/spotify/callback', passport.authenticate('spotify', { failureRedirect: '/login' }), (req, res) => {
    if (req.user) {
      config.api.token = req.token;
      getTopTrack().then((topTrack) => {
        app.set('trackId', topTrack.uri);
        res.redirect('/thank-you');
      });
      saveData(config.collections);
      // TODO: move saving respondent out of /spotify
      const status = writeCSV({ userId: app.get('userId'), date: new Date() }, 'respondents');
      logger.info(status);
    } else {
      res.status(401).send({ error: 'Error logging in!' });
    }
  });
};
