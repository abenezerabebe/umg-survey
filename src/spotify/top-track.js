import { apiRequest } from '../util';

export const getTopTrack = async () => {
  const { items } = await apiRequest({
    endpoint: '/top/tracks',
    query: { limit: '1', time_range: 'long_term' }
  });
  return items[0];
};
