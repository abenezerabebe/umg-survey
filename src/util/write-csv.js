import json2csv from 'json2csv';
import fs from 'fs';
import logger from './logger';

const newLine = '\r\n';
/**
 * Creates and writes a CSV file.
 * @param {array | object} data - JSON object to write to CSV.
 * @param {*} name -The name of the file to write.
 */
const writeCSV = (data, name) => (
  new Promise((resolve, reject) => {
    const csv = fs.existsSync(`./data/${name}.csv`) ? json2csv({ data, hasCSVColumnTitle: false }) : json2csv({ data });
    const wstream = fs.createWriteStream(`./data/${name}.csv`, { flags: 'a' });

    wstream.on('finish', () => {
      logger.info(`Wrote ${name}`);
      resolve({ name, result: true });
    });

    wstream.on('error', (err) => {
      logger.error('Error during request: ', err);
      reject(err);
    });
    wstream.write(csv + newLine);
    wstream.end();
  })
);

export default writeCSV;
