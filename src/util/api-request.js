import rp from 'request-promise';
import logger from './logger';
import { spotify as config } from '../../config';

export const apiRequest = async (request) => {
  const options = {
    uri: `${config.api.baseURL}${request.endpoint}`,
    headers: { Authorization: `Bearer ${config.api.token}` },
    qs: request.query,
    json: true
  };
  let val;
  try {
    val = await rp(options);
  } catch (e) {
    logger.error(e);
    return null;
  }
  return val;
};
