import logger from './logger';

export default handler =>
  (req, res) =>
    handler(req, res)
      .catch(err => logger.error('Error during request: ', err));
