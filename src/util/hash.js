import crypto from 'crypto';
import { auth as authConfig } from '../../config';

export default (str) => {
  const hash = crypto.createHash('sha256');
  hash.update(str + authConfig.passwordSalt);
  return hash.digest('hex');
};
