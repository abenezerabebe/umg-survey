import R from 'ramda';

const { isArray } = Array;

/**
 * Merge 2 objects into one by joining common properties into string.
 * @param {string} k - The common key between objects.
 * @param {object} param1 - First object to merge, destructured by common key.
 * @param {object} param2 - Second object to merge, destructured by common key.
 */
const mergeObj = (k, { [k]: a, ...rest1 }, { [k]: b, ...rest2 }) => (
  { [k]: `${a}, ${b}`, ...rest1, ...rest2 }
);

/**
 * Get common keys between two objects.
 * @param {*} a - First object
 * @param {*} b - Second objrct
 */
const getCommonKeys = (a, b) => (
  Object.keys(b).length < Object.keys(a)
    ? Object.keys(b).filter(k => a[k])
    : Object.keys(a).filter(k => b[k])
);

/**
 * Reduce items in array to object.
 * @param {array} arr - The array to reduce.
 */
const arrToObj = arr => (
  arr.reduce((a, b) => {
    const [commonKey] = getCommonKeys(a, b);
    return a[commonKey]
      ? mergeObj(commonKey, a, b)
      : { ...a, ...b };
  })
);

/**
 * Reformat an object.
 * @param {object} map: - The reformatting structure.
 * @param {object} n: - The object to reformat.
 */
export const mapper = (map, n) => (
  Object.keys(map).reduce((acc, key) => {
    const val = R.path(key.split('.'), n);
    if (isArray(val)) {
      return [acc, ...val.map(v => mapper(map[key], v))];
    }
    if (isArray(acc)) {
      return { ...arrToObj(acc), [map[key]]: val };
    }
    return { ...acc, [map[key]]: val };
  }, {})
);
