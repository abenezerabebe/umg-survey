import app from './app';
import logger from './util/logger';
import { thinky } from './db';

const PORT = process.env.PORT || 8080;

// wait for DB to init
thinky.dbReady().then(() => {
  logger.info('Database ready, starting server...');
  // start server
  app.listen(PORT, () => {
    logger.info(`Server is listening at port ${PORT}`);
  });

  // output uncaught exceptions
  process.on('uncaughtException', err => logger.info('uncaught exception: ', err));
  process.on('unhandledRejection', err => logger.info('unhandled rejection: ', err));
});
