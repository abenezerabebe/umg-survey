// npm packages
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { Strategy as SpotifyStrategy } from 'passport-spotify';

// proj packages
import { User } from '../db';
import { hash } from '../util';
import { auth as authConfig, spotify as spotifyConfig } from '../../config';

// define serialize and deserialize functions
passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser(async (obj, done) => {
  if (obj.provider) return done(null, obj);
  let user = null;
  user = await User.get(obj.id)
    .without(['password'])
    .execute();
  try {
    user = await User.get(obj.id)
      .without(['password'])
      .execute();
  } catch (e) {
    return done(e, false);
  }

  return done(null, user);
});

// use LocalStrategy
passport.use(new LocalStrategy({ usernameField: 'username' }, async (username, password, done) => {
  // find all users with matching username
  let users = [];
  try {
    users = await User.filter({ username }).limit(1).run();
  } catch (e) {
    return done(e, false);
  }
  // get the first match
  const user = users[0];
  // check if exists
  if (!user) {
    return done(null, false);
  }
  // compare password
  if (user.password !== hash(password)) {
    return done(null, false);
  }
  // return user if successful
  delete user.password;
  return done(null, user);
}));

// use JWTStrategy
const jwtOpts = {
  jwtFromRequest: ExtractJwt.fromHeader('x-access-token'),
  secretOrKey: authConfig.jwtSecret,
};
passport.use(new JwtStrategy(jwtOpts, async (payload, done) => {
  let user;
  try {
    user = await User.get(payload.id)
      .without(['password'])
      .execute();
  } catch (e) {
    return done(e, false);
  }
  // check if exists
  if (!user) {
    return done(null, false);
  }
  // return user if successful
  return done(null, user);
}));

//  use Spotify Strategy
const spotifyOpts = {
  clientID: spotifyConfig.spotifyClientId,
  clientSecret: spotifyConfig.spotifyClientSecret,
  callbackURL: spotifyConfig.spotifyCallbackUrl,
  passReqToCallback: true,
};
passport.use(new SpotifyStrategy(
  spotifyOpts,
  async (req, accessToken, refreshToken, profile, done) => {
    // pass token to req
    req.token = accessToken;
    // return token if successful
    return done(null, profile);
  },
));
