// our packages
import { User } from '../db';
import { hash, asyncRequest } from '../util';

export const usernameTaken = async (username) => {
  // check if username already taken
  const users = await User.filter({ username }).run();
  return users.length > 0;
};

export default (app) => {
  app.post('/api/register', asyncRequest(async (req, res) => {
    // get user input
    const { username, password, passwordRepeat } = req.body;

    if (password !== passwordRepeat) {
      res.status(400).send({ error: 'Passwords do not match!' });
      return;
    }
    // hash password
    const hashedPassword = hash(password);

    // check if username already taken
    const exists = await usernameTaken(username);
    if (exists) {
      res.status(403).send({ error: 'User already exists!' });
      return;
    }

    // save new user
    const user = new User({
      username,
      password: hashedPassword,
    });
    await user.save();

    res.sendStatus(201);
  }));
};
