// npm packages
import test from 'tape';

// local packages
import { thinky } from '../src/db';

// tests
import core from './core';
import register from './register';
// import login from './login';
// import user from './user';

thinky.dbReady().then(() => {
  // execute tests
  core(test);
  register(test);
  // login(test);
  // user(test);

  // close db connections
  test((t) => {
    setImmediate(() => thinky.r.getPoolMaster().drain());
    t.end();
  });
});
