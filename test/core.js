// npm packages
import request from 'supertest';

// our packages
import app from '../src/app';

export default (test) => {
  test('GET /', (t) => {
    request(app)
      .get('/')
      .expect(302)
      .end((err) => {
        t.error(err, 'No error');
        t.end();
      });
  });

  test('GET / with query string param', (t) => {
    request(app)
      .get('/?id=12345')
      .expect(302)
      .end((err) => {
        const userId = app.get('userId');
        t.error(err, 'No error');
        t.equal(userId, '12345', 'id matches request');
        t.end();
      });
  });

  test('GET / set user to test if without qs', (t) => {
    request(app)
      .get('/')
      .expect(302)
      .end((err) => {
        const userId = app.get('userId');
        t.error(err, 'No error');
        t.equal(userId, 'test', 'id matches request');
        t.end();
      });
  });

  // test('404 on nonexistant URL', (t) => {
  //   request(app)
  //     .get('/GETShouldFailOnRandomURL')
  //     .expect(404)
  //     .expect('Content-Type', /text\/html/)
  //     .end((err, res) => {
  //       const expectedBody = 'Cannot GET /GETShouldFailOnRandomURL\n';
  //       const actualBody = res.text;
  //
  //       t.error(err, 'No error');
  //       t.equal(actualBody, expectedBody, 'Retrieve body');
  //       t.end();
  //     });
  // });
};
