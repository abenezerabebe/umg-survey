exports.db = {
  host: process.env.EXPERTS_DB_URL || 'localhost',
  port: process.env.EXPERTS_DB_PORT || 28015,
  db: 'expertsdb',
};

exports.auth = {
  passwordSalt: '+q*?fy$pwfvJ]9}#As!*C(zX@K4nc$:Z6V@9?gF{[?7K#~)H["[}N(J!~#S<c2X&',
  sessionSecret: 'CVF6NqLg7#Kyd7L8rN%$*D4z_F+%xL5Y!_^*M_ZC?eq^&2g&4VLMq##ZRE@5C$H5',
  jwtSecret: 'Q%F&c@9f$2!3gBWSg5!9LeE$M3bhcy#!zy-kYV2LrStMZP!srDu2$BB%BRB6kG5q',
};

exports.spotify = {
  spotifyClientId: '40ff6e5592fe4b6793b0637d114bc3fa',
  spotifyClientSecret: 'c6c965a6b8be46c689ab7069b7b72b11',
  spotifyCallbackUrl: process.env.CALLBACK_URL || 'http://localhost:8080/api/auth/spotify/callback',
  scope: ['playlist-read-private', 'user-library-read', 'user-top-read', 'user-follow-read', 'user-read-recently-played', 'user-read-playback-state', 'user-read-currently-playing'],
  collections: [
    {
      name: 'saved-tracks',
      request: {
        endpoint: '/tracks',
        query: { limit: '1' }
      },
      map: {
        items: {
          added_at: 'added_at',
          'track.artists': { name: 'track_artists' },
          'track.album.name': 'album_name',
          'track.name': 'track_name'
        }
      }
    },
    {
      name: 'top-tracks',
      request: {
        endpoint: '/top/tracks',
        query: { limit: '1', time_range: 'long_term' }
      },
      map: {
        items: {
          'album.name': 'album',
          artists: { name: 'track_artists' },
          duration_ms: 'duration_ms',
          name: 'name',
          popularity: 'popularity'
        }
      }
    },
    {
      name: 'top-artists',
      request: {
        endpoint: '/top/artists',
        query: { limit: '1', time_range: 'long_term' }
      },
      map: {
        items: {
          'followers.total': 'followers',
          name: 'name',
          popularity: 'popularity'
        }
      }
    },
    {
      name: 'recently-played',
      request: {
        endpoint: '/player/recently-played',
        query: { limit: '1' }
      },
      map: {
        items: {
          'track.name': 'track_name',
          'track.artists': { name: 'track_artists' },
          'track.duration_ms': 'duration_ms',
          played_at: 'played_at'
        }
      }
    },
    {
      name: 'current-playback',
      request: {
        endpoint: '/player'
      },
      map: {
        'device.name': 'device_name',
        'device.type': 'device_type',
        'device.volume_percent': 'volume_percent',
        repeat_state: 'repeat_state',
        shuffle_state: 'shuffle_state',
        timestamp: 'timestamp',
        progress_ms: 'progress_ms',
        is_playing: 'is_playing'
      }
    },
    {
      name: 'devices',
      request: {
        endpoint: '/player/devices'
      },
      map: {
        devices: {
          name: 'name',
          type: 'type',
          volume_percent: 'volume_percent'
        }
      }
    },
    {
      name: 'currently-playing',
      request: {
        endpoint: '/player/currently-playing'
      },
      map: {
        'item.name': 'track_name',
        'item.artists': { name: 'track_artists' },
        timestamp: 'timestamp',
        progress_ms: 'progress_ms',
        is_playing: 'is_playing'
      }
    },
    {
      name: 'playlists',
      request: {
        endpoint: '/playlists',
        query: { limit: '1' }
      },
      map: {
        items: {
          collaborative: 'is-collaborative',
          name: 'name',
          public: 'is-public',
          'tracks.total': 'tracks',
          'owner.id': 'owner'
        }
      }
    }
  ],
  api: {
    token: '',
    baseURL: 'https://api.spotify.com/v1/me'
  }
};
